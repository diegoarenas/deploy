(function () {
    'use strict';

    angular
        .module('genteraApp')
        .config(materialDesignConfig);

    function materialDesignConfig() {
        // Initialize material design
        $.material.init();
    }
})();
