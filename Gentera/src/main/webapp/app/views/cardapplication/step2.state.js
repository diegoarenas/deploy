(function() {
    'use strict';

    angular
        .module('genteraApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider','$urlRouterProvider'];

    function stateConfig($stateProvider,$urlRouterProvider) {
        $stateProvider.state('step2', {
            parent: 'views',
            url: '/step2',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/views/cardapplication/step2.html',
                    controller: 'Step2Controller',
                    controllerAs: 'vm'
                }
            }
        })
    }
})();
