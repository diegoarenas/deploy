package com.capgemini.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Customer.
 */
@Entity
@Table(name = "customer")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "suffix")
    private String suffix;

    @Column(name = "email")
    private String email;

    @Column(name = "primary_phone")
    private Long primaryPhone;

    @Column(name = "dob")
    private LocalDate dob;

    @Column(name = "ssn")
    private String ssn;

    @Column(name = "emp_status")
    private String empStatus;

    @Column(name = "total_gross_income")
    private String totalGrossIncome;

    @Column(name = "house_rent")
    private String houseRent;

    @Column(name = "total_assets")
    private String totalAssets;

    @Column(name = "occupation")
    private String occupation;

    @Column(name = "exp")
    private String exp;

    @Column(name = "qualification")
    private String qualification;

    @Column(name = "housing_status")
    private String housingStatus;

    @Column(name = "having_debit_acc")
    private Boolean havingDebitAcc;

    @OneToMany(mappedBy = "customer")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Address> addresses = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getPrimaryPhone() {
        return primaryPhone;
    }

    public void setPrimaryPhone(Long primaryPhone) {
        this.primaryPhone = primaryPhone;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getEmpStatus() {
        return empStatus;
    }

    public void setEmpStatus(String empStatus) {
        this.empStatus = empStatus;
    }

    public String getTotalGrossIncome() {
        return totalGrossIncome;
    }

    public void setTotalGrossIncome(String totalGrossIncome) {
        this.totalGrossIncome = totalGrossIncome;
    }

    public String getHouseRent() {
        return houseRent;
    }

    public void setHouseRent(String houseRent) {
        this.houseRent = houseRent;
    }

    public String getTotalAssets() {
        return totalAssets;
    }

    public void setTotalAssets(String totalAssets) {
        this.totalAssets = totalAssets;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getHousingStatus() {
        return housingStatus;
    }

    public void setHousingStatus(String housingStatus) {
        this.housingStatus = housingStatus;
    }

    public Boolean isHavingDebitAcc() {
        return havingDebitAcc;
    }

    public void setHavingDebitAcc(Boolean havingDebitAcc) {
        this.havingDebitAcc = havingDebitAcc;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Customer customer = (Customer) o;
        if(customer.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, customer.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Customer{" +
            "id=" + id +
            ", firstName='" + firstName + "'" +
            ", lastName='" + lastName + "'" +
            ", middleName='" + middleName + "'" +
            ", suffix='" + suffix + "'" +
            ", email='" + email + "'" +
            ", primaryPhone='" + primaryPhone + "'" +
            ", dob='" + dob + "'" +
            ", ssn='" + ssn + "'" +
            ", empStatus='" + empStatus + "'" +
            ", totalGrossIncome='" + totalGrossIncome + "'" +
            ", houseRent='" + houseRent + "'" +
            ", totalAssets='" + totalAssets + "'" +
            ", occupation='" + occupation + "'" +
            ", exp='" + exp + "'" +
            ", qualification='" + qualification + "'" +
            ", housingStatus='" + housingStatus + "'" +
            ", havingDebitAcc='" + havingDebitAcc + "'" +
            '}';
    }
}
