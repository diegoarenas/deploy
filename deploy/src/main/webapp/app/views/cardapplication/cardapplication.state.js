(function() {
    'use strict';

    angular
        .module('banorteApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider','$urlRouterProvider'];

    function stateConfig($stateProvider,$urlRouterProvider) {
        $stateProvider.state('cardapplication', {
            parent: 'views',
            url: '/cardapplication',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/views/cardapplication/cardapplication.html',
                    controller: 'CardApplicationController',
                    controllerAs: 'vm'
                }
            }
        })
    }
})();
