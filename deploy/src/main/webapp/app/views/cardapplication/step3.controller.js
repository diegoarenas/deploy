(function() {
    'use strict';

    angular
        .module('banorteApp')
        .controller('Step3Controller', Step3Controller);

    Step3Controller.$inject = ['$scope', '$state','$localStorage'];

    function Step3Controller ($scope, $state,$localStorage) {
        var vm = this;
        vm.imageName=$localStorage.imgSrc;
        vm.userName=$localStorage.fName;
    }
})();
