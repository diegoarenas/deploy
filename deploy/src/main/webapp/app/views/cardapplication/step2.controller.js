(function() {
    'use strict';

    angular
        .module('banorteApp')
        .controller('Step2Controller', Step2Controller);

    Step2Controller.$inject = ['$scope', '$state','$localStorage','$window','$location','$rootScope'];

    function Step2Controller ($scope, $state,$localStorage,$window,$location,$rootScope) {
        var vm = this;
        vm.imageName=$localStorage.imgSrc;
        vm.userName=$localStorage.fName;
    }
})();
