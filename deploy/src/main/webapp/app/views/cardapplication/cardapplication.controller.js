(function() {
    'use strict';

    angular
        .module('banorteApp')
        .controller('CardApplicationController', CardApplicationController);

    CardApplicationController.$inject = ['$scope', '$state','$localStorage','$window','$location','$rootScope'];

    function CardApplicationController ($scope, $state,$localStorage,$window,$location,$rootScope) {
        var vm = this;
        vm.imgName="1";
        vm.userName=vm.firstName;
        vm.changeImg=function(id)
        {
            if(id==1)
                vm.imgName="1";
            if(id==2)
                vm.imgName="2";
            if(id==3)
                vm.imgName="3";
            if(id==4)
                vm.imgName="4";
            if(id==5)
                vm.imgName="5";
            if(id==6)
                vm.imgName="6";
            if(id==7)
                vm.imgName="7";
        }
        vm.next=  function(){
            $localStorage.fName=vm.firstName;
            $localStorage.imgSrc=vm.imgName;
            $location.path('/step2');
        }
    }
})();
