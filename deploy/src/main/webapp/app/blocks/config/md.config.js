(function () {
    'use strict';

    angular
        .module('banorteApp')
        .config(materialDesignConfig);

    function materialDesignConfig() {
        // Initialize material design
        $.material.init();
    }
})();
