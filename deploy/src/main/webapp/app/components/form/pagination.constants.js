(function() {
    'use strict';

    angular
        .module('banorteApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
