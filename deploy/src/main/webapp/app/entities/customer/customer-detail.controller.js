(function() {
    'use strict';

    angular
        .module('banorteApp')
        .controller('CustomerDetailController', CustomerDetailController);

    CustomerDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Customer', 'Address'];

    function CustomerDetailController($scope, $rootScope, $stateParams, entity, Customer, Address) {
        var vm = this;

        vm.customer = entity;

        var unsubscribe = $rootScope.$on('banorteApp:customerUpdate', function(event, result) {
            vm.customer = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
