(function() {
    'use strict';

    angular
        .module('banorteApp')
        .controller('AddressDetailController', AddressDetailController);

    AddressDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Address', 'Customer'];

    function AddressDetailController($scope, $rootScope, $stateParams, entity, Address, Customer) {
        var vm = this;

        vm.address = entity;

        var unsubscribe = $rootScope.$on('banorteApp:addressUpdate', function(event, result) {
            vm.address = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
