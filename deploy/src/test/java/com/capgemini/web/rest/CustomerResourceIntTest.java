package com.capgemini.web.rest;

import com.capgemini.BanorteApp;
import com.capgemini.domain.Customer;
import com.capgemini.repository.CustomerRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the CustomerResource REST controller.
 *
 * @see CustomerResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BanorteApp.class)
@WebAppConfiguration
@IntegrationTest
public class CustomerResourceIntTest {

    private static final String DEFAULT_FIRST_NAME = "AAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBB";
    private static final String DEFAULT_LAST_NAME = "AAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBB";
    private static final String DEFAULT_MIDDLE_NAME = "AAAAA";
    private static final String UPDATED_MIDDLE_NAME = "BBBBB";
    private static final String DEFAULT_SUFFIX = "AAAAA";
    private static final String UPDATED_SUFFIX = "BBBBB";
    private static final String DEFAULT_EMAIL = "AAAAA";
    private static final String UPDATED_EMAIL = "BBBBB";

    private static final Long DEFAULT_PRIMARY_PHONE = 1L;
    private static final Long UPDATED_PRIMARY_PHONE = 2L;

    private static final LocalDate DEFAULT_DOB = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DOB = LocalDate.now(ZoneId.systemDefault());
    private static final String DEFAULT_SSN = "AAAAA";
    private static final String UPDATED_SSN = "BBBBB";
    private static final String DEFAULT_EMP_STATUS = "AAAAA";
    private static final String UPDATED_EMP_STATUS = "BBBBB";
    private static final String DEFAULT_TOTAL_GROSS_INCOME = "AAAAA";
    private static final String UPDATED_TOTAL_GROSS_INCOME = "BBBBB";
    private static final String DEFAULT_HOUSE_RENT = "AAAAA";
    private static final String UPDATED_HOUSE_RENT = "BBBBB";
    private static final String DEFAULT_TOTAL_ASSETS = "AAAAA";
    private static final String UPDATED_TOTAL_ASSETS = "BBBBB";
    private static final String DEFAULT_OCCUPATION = "AAAAA";
    private static final String UPDATED_OCCUPATION = "BBBBB";
    private static final String DEFAULT_EXP = "AAAAA";
    private static final String UPDATED_EXP = "BBBBB";
    private static final String DEFAULT_QUALIFICATION = "AAAAA";
    private static final String UPDATED_QUALIFICATION = "BBBBB";
    private static final String DEFAULT_HOUSING_STATUS = "AAAAA";
    private static final String UPDATED_HOUSING_STATUS = "BBBBB";

    private static final Boolean DEFAULT_HAVING_DEBIT_ACC = false;
    private static final Boolean UPDATED_HAVING_DEBIT_ACC = true;

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restCustomerMockMvc;

    private Customer customer;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CustomerResource customerResource = new CustomerResource();
        ReflectionTestUtils.setField(customerResource, "customerRepository", customerRepository);
        this.restCustomerMockMvc = MockMvcBuilders.standaloneSetup(customerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        customer = new Customer();
        customer.setFirstName(DEFAULT_FIRST_NAME);
        customer.setLastName(DEFAULT_LAST_NAME);
        customer.setMiddleName(DEFAULT_MIDDLE_NAME);
        customer.setSuffix(DEFAULT_SUFFIX);
        customer.setEmail(DEFAULT_EMAIL);
        customer.setPrimaryPhone(DEFAULT_PRIMARY_PHONE);
        customer.setDob(DEFAULT_DOB);
        customer.setSsn(DEFAULT_SSN);
        customer.setEmpStatus(DEFAULT_EMP_STATUS);
        customer.setTotalGrossIncome(DEFAULT_TOTAL_GROSS_INCOME);
        customer.setHouseRent(DEFAULT_HOUSE_RENT);
        customer.setTotalAssets(DEFAULT_TOTAL_ASSETS);
        customer.setOccupation(DEFAULT_OCCUPATION);
        customer.setExp(DEFAULT_EXP);
        customer.setQualification(DEFAULT_QUALIFICATION);
        customer.setHousingStatus(DEFAULT_HOUSING_STATUS);
        customer.setHavingDebitAcc(DEFAULT_HAVING_DEBIT_ACC);
    }

    @Test
    @Transactional
    public void createCustomer() throws Exception {
        int databaseSizeBeforeCreate = customerRepository.findAll().size();

        // Create the Customer

        restCustomerMockMvc.perform(post("/api/customers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(customer)))
                .andExpect(status().isCreated());

        // Validate the Customer in the database
        List<Customer> customers = customerRepository.findAll();
        assertThat(customers).hasSize(databaseSizeBeforeCreate + 1);
        Customer testCustomer = customers.get(customers.size() - 1);
        assertThat(testCustomer.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testCustomer.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testCustomer.getMiddleName()).isEqualTo(DEFAULT_MIDDLE_NAME);
        assertThat(testCustomer.getSuffix()).isEqualTo(DEFAULT_SUFFIX);
        assertThat(testCustomer.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testCustomer.getPrimaryPhone()).isEqualTo(DEFAULT_PRIMARY_PHONE);
        assertThat(testCustomer.getDob()).isEqualTo(DEFAULT_DOB);
        assertThat(testCustomer.getSsn()).isEqualTo(DEFAULT_SSN);
        assertThat(testCustomer.getEmpStatus()).isEqualTo(DEFAULT_EMP_STATUS);
        assertThat(testCustomer.getTotalGrossIncome()).isEqualTo(DEFAULT_TOTAL_GROSS_INCOME);
        assertThat(testCustomer.getHouseRent()).isEqualTo(DEFAULT_HOUSE_RENT);
        assertThat(testCustomer.getTotalAssets()).isEqualTo(DEFAULT_TOTAL_ASSETS);
        assertThat(testCustomer.getOccupation()).isEqualTo(DEFAULT_OCCUPATION);
        assertThat(testCustomer.getExp()).isEqualTo(DEFAULT_EXP);
        assertThat(testCustomer.getQualification()).isEqualTo(DEFAULT_QUALIFICATION);
        assertThat(testCustomer.getHousingStatus()).isEqualTo(DEFAULT_HOUSING_STATUS);
        assertThat(testCustomer.isHavingDebitAcc()).isEqualTo(DEFAULT_HAVING_DEBIT_ACC);
    }

    @Test
    @Transactional
    public void getAllCustomers() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customers
        restCustomerMockMvc.perform(get("/api/customers?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(customer.getId().intValue())))
                .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME.toString())))
                .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())))
                .andExpect(jsonPath("$.[*].middleName").value(hasItem(DEFAULT_MIDDLE_NAME.toString())))
                .andExpect(jsonPath("$.[*].suffix").value(hasItem(DEFAULT_SUFFIX.toString())))
                .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
                .andExpect(jsonPath("$.[*].primaryPhone").value(hasItem(DEFAULT_PRIMARY_PHONE.intValue())))
                .andExpect(jsonPath("$.[*].dob").value(hasItem(DEFAULT_DOB.toString())))
                .andExpect(jsonPath("$.[*].ssn").value(hasItem(DEFAULT_SSN.toString())))
                .andExpect(jsonPath("$.[*].empStatus").value(hasItem(DEFAULT_EMP_STATUS.toString())))
                .andExpect(jsonPath("$.[*].totalGrossIncome").value(hasItem(DEFAULT_TOTAL_GROSS_INCOME.toString())))
                .andExpect(jsonPath("$.[*].houseRent").value(hasItem(DEFAULT_HOUSE_RENT.toString())))
                .andExpect(jsonPath("$.[*].totalAssets").value(hasItem(DEFAULT_TOTAL_ASSETS.toString())))
                .andExpect(jsonPath("$.[*].occupation").value(hasItem(DEFAULT_OCCUPATION.toString())))
                .andExpect(jsonPath("$.[*].exp").value(hasItem(DEFAULT_EXP.toString())))
                .andExpect(jsonPath("$.[*].qualification").value(hasItem(DEFAULT_QUALIFICATION.toString())))
                .andExpect(jsonPath("$.[*].housingStatus").value(hasItem(DEFAULT_HOUSING_STATUS.toString())))
                .andExpect(jsonPath("$.[*].havingDebitAcc").value(hasItem(DEFAULT_HAVING_DEBIT_ACC.booleanValue())));
    }

    @Test
    @Transactional
    public void getCustomer() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get the customer
        restCustomerMockMvc.perform(get("/api/customers/{id}", customer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(customer.getId().intValue()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME.toString()))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME.toString()))
            .andExpect(jsonPath("$.middleName").value(DEFAULT_MIDDLE_NAME.toString()))
            .andExpect(jsonPath("$.suffix").value(DEFAULT_SUFFIX.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.primaryPhone").value(DEFAULT_PRIMARY_PHONE.intValue()))
            .andExpect(jsonPath("$.dob").value(DEFAULT_DOB.toString()))
            .andExpect(jsonPath("$.ssn").value(DEFAULT_SSN.toString()))
            .andExpect(jsonPath("$.empStatus").value(DEFAULT_EMP_STATUS.toString()))
            .andExpect(jsonPath("$.totalGrossIncome").value(DEFAULT_TOTAL_GROSS_INCOME.toString()))
            .andExpect(jsonPath("$.houseRent").value(DEFAULT_HOUSE_RENT.toString()))
            .andExpect(jsonPath("$.totalAssets").value(DEFAULT_TOTAL_ASSETS.toString()))
            .andExpect(jsonPath("$.occupation").value(DEFAULT_OCCUPATION.toString()))
            .andExpect(jsonPath("$.exp").value(DEFAULT_EXP.toString()))
            .andExpect(jsonPath("$.qualification").value(DEFAULT_QUALIFICATION.toString()))
            .andExpect(jsonPath("$.housingStatus").value(DEFAULT_HOUSING_STATUS.toString()))
            .andExpect(jsonPath("$.havingDebitAcc").value(DEFAULT_HAVING_DEBIT_ACC.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCustomer() throws Exception {
        // Get the customer
        restCustomerMockMvc.perform(get("/api/customers/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCustomer() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);
        int databaseSizeBeforeUpdate = customerRepository.findAll().size();

        // Update the customer
        Customer updatedCustomer = new Customer();
        updatedCustomer.setId(customer.getId());
        updatedCustomer.setFirstName(UPDATED_FIRST_NAME);
        updatedCustomer.setLastName(UPDATED_LAST_NAME);
        updatedCustomer.setMiddleName(UPDATED_MIDDLE_NAME);
        updatedCustomer.setSuffix(UPDATED_SUFFIX);
        updatedCustomer.setEmail(UPDATED_EMAIL);
        updatedCustomer.setPrimaryPhone(UPDATED_PRIMARY_PHONE);
        updatedCustomer.setDob(UPDATED_DOB);
        updatedCustomer.setSsn(UPDATED_SSN);
        updatedCustomer.setEmpStatus(UPDATED_EMP_STATUS);
        updatedCustomer.setTotalGrossIncome(UPDATED_TOTAL_GROSS_INCOME);
        updatedCustomer.setHouseRent(UPDATED_HOUSE_RENT);
        updatedCustomer.setTotalAssets(UPDATED_TOTAL_ASSETS);
        updatedCustomer.setOccupation(UPDATED_OCCUPATION);
        updatedCustomer.setExp(UPDATED_EXP);
        updatedCustomer.setQualification(UPDATED_QUALIFICATION);
        updatedCustomer.setHousingStatus(UPDATED_HOUSING_STATUS);
        updatedCustomer.setHavingDebitAcc(UPDATED_HAVING_DEBIT_ACC);

        restCustomerMockMvc.perform(put("/api/customers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedCustomer)))
                .andExpect(status().isOk());

        // Validate the Customer in the database
        List<Customer> customers = customerRepository.findAll();
        assertThat(customers).hasSize(databaseSizeBeforeUpdate);
        Customer testCustomer = customers.get(customers.size() - 1);
        assertThat(testCustomer.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testCustomer.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testCustomer.getMiddleName()).isEqualTo(UPDATED_MIDDLE_NAME);
        assertThat(testCustomer.getSuffix()).isEqualTo(UPDATED_SUFFIX);
        assertThat(testCustomer.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testCustomer.getPrimaryPhone()).isEqualTo(UPDATED_PRIMARY_PHONE);
        assertThat(testCustomer.getDob()).isEqualTo(UPDATED_DOB);
        assertThat(testCustomer.getSsn()).isEqualTo(UPDATED_SSN);
        assertThat(testCustomer.getEmpStatus()).isEqualTo(UPDATED_EMP_STATUS);
        assertThat(testCustomer.getTotalGrossIncome()).isEqualTo(UPDATED_TOTAL_GROSS_INCOME);
        assertThat(testCustomer.getHouseRent()).isEqualTo(UPDATED_HOUSE_RENT);
        assertThat(testCustomer.getTotalAssets()).isEqualTo(UPDATED_TOTAL_ASSETS);
        assertThat(testCustomer.getOccupation()).isEqualTo(UPDATED_OCCUPATION);
        assertThat(testCustomer.getExp()).isEqualTo(UPDATED_EXP);
        assertThat(testCustomer.getQualification()).isEqualTo(UPDATED_QUALIFICATION);
        assertThat(testCustomer.getHousingStatus()).isEqualTo(UPDATED_HOUSING_STATUS);
        assertThat(testCustomer.isHavingDebitAcc()).isEqualTo(UPDATED_HAVING_DEBIT_ACC);
    }

    @Test
    @Transactional
    public void deleteCustomer() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);
        int databaseSizeBeforeDelete = customerRepository.findAll().size();

        // Get the customer
        restCustomerMockMvc.perform(delete("/api/customers/{id}", customer.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Customer> customers = customerRepository.findAll();
        assertThat(customers).hasSize(databaseSizeBeforeDelete - 1);
    }
}
