(function() {
    'use strict';

    angular
        .module('genteraApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
