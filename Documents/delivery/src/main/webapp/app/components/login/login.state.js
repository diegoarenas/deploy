/**
 * Created by adhabale on 3/1/2017.
 */
(function() {
    'use strict';

    angular
        .module('genteraApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider','$urlRouterProvider'];

    function stateConfig($stateProvider,$urlRouterProvider) {
        $stateProvider.state('login', {
            url: '/login',
            data: {
                authorities: [],
                pageTitle: 'Login'
            },
            views: {
                'content@': {
                    templateUrl: 'app/components/login/login.html',
                    controller: 'LoginController',
                    controllerAs: 'vm'
                }
            }
        });
        $urlRouterProvider.otherwise('login');
    }
})();
