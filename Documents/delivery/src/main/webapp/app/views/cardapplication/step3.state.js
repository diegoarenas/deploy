(function() {
    'use strict';

    angular
        .module('genteraApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider','$urlRouterProvider'];

    function stateConfig($stateProvider,$urlRouterProvider) {
        $stateProvider.state('step3', {
            parent: 'views',
            url: '/step3',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/views/cardapplication/step3.html',
                    controller: 'Step3Controller',
                    controllerAs: 'vm'
                }
            }
        })
    }
})();
